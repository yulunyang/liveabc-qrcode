import moment from 'moment'

export default {

  props: {
    url: {
      type: String,
      required: true,
      default: ''
    },
    subtitles: {
      type: Array,
      required: false,
      default: () => {
        return []
      }
    },
    playAt: {
      type: Number,
      required: false,
      default: -1
    },
    control: {
      type: String,
      required: false,
      default: ''
    }
  },

  data () {
    return {
      id: -1,
      video: {
        el: null,
        url: '',
        state: 'stop',
        currentPos: 0,
        currentTime: 0,
        duration: 0,
        fullscreen: false,
        cc: 0,
        currentSentIdx: 0,
        pos: 0,
        slow: false
      },

      repeat: false,
      repeatIdx: -1,
      times: 0,
      isFullscreen: false
    }
  },

  created () {
    this.id = this._uid
    this.video.url = this.url
  },

  beforeMount () {
    this.$watch('playAt', (val, old) => {
      if (val >= 0) {
        this.repeatIdx = val // this.findSentIdx(val / 1000)
        this.times = 1
        this.jumpToSent(this.subtitles[val])
        this.play()
      } else {
        this.pause()
      }
    })

    this.$watch('url', (val, old) => {
      this.pause()
      this.video.url = this.url
      this.video.currentPos = 0
      this.video.duration = 0
      this.video.currentTime = 0
    })

    this.$watch('control', (val, old) => {
      switch (val) {
        case 'play':
          this.play()
          break
        case 'pause':
          this.pause()
          break
      }
    })
  },

  mounted () {
    this.$nextTick(() => {
      var self = this
      // this.video.el = document.getElementsByTagName('video')[0] || document.getElementsByTagName('video')
      this.video.el = this.$refs.video
      this.video.el.addEventListener('timeupdate', function () {
        if (self.video.pos > 0 && this.duration) {
          this.currentTime = self.video.pos / 100 * this.duration
          self.video.pos = 0
        }

        self.video.currentPos = (this.currentTime / this.duration) * 100
        self.video.duration = this.duration
        self.video.currentTime = this.currentTime
        self.video.currentSentIdx = self.findSentIdx(this.currentTime)
        self.$emit('video', self.video)

        var MtcEnd = 0
        // repeat
        if (self.repeat && self.subtitles.length > 0) {
          MtcEnd = self.video.slow ? self.subtitles[self.repeatIdx].timeCode_Slow.end / 1000 : self.subtitles[self.repeatIdx].timeCode_Normal.end / 1000
          if (this.currentTime >= MtcEnd) {
            self.jumpToSent(self.subtitles[self.repeatIdx])
          }
        }

        // times
        if (self.times > 0) {
          MtcEnd = self.video.slow ? self.subtitles[self.repeatIdx].timeCode_Slow.end / 1000 : self.subtitles[self.repeatIdx].timeCode_Normal.end / 1000
          if (this.currentTime >= MtcEnd) {
            (--self.times > 0) ? self.jumpToSent(self.subtitles[self.repeatIdx]) : self.pause()
          }
        }
      })

      this.video.el.onplaying = function () {
        self.video.state = 'playing'
        self.$emit('video', self.video)
      }

      this.video.el.onpause = function () {
        self.video.state = 'stop'
        self.$emit('video', self.video)
      }

      this.video.el.onended = function () {
        if (self.repeat && self.subtitles.length === 0) {
          self.play(0)
        } else {
          self.video.el.currentTime = 0
        }
      }
    })
  },

  beforeDestroy () {
    window.removeEventListener('keyup', this.handleKeyup)
    this.video.el.onplaying = null
    this.video.el.onpause = null
    this.video.el.timeupdate = null
  },

  methods: {
    play (seconds) {
      seconds = seconds || -1
      if (seconds >= 0) {
        this.video.el.currentTime = seconds
      }

      if (this.video.state !== 'playing') {
        this.video.el.play()
        this.video.state = 'playing'
      }
    },

    pause () {
      if (this.video.state !== 'stop') {
        this.video.el.pause()
        this.video.state = 'stop'
      }
    },

    toggle () {
      if (!this.video.el) {
        throw new Error('media element not set.')
      }

      (this.video.state === 'playing') ? this.pause() : this.play()
    },

    duration: function (seconds) {
      return moment.duration(seconds, 'minutes').format('hh:mm', { trim: false })
    },

    seekTo (newPosition) {
      this.video.currentPos = newPosition
      this.video.el.currentTime = (this.video.currentPos / 100) * this.video.duration
    },

    findSentIdx (time) {
      for (var i = 0; i < this.subtitles.length; i++) {
        var begin = this.video.slow ? this.subtitles[i].timeCode_Slow.begin / 1000 : this.subtitles[i].timeCode_Normal.begin / 1000
        var end = this.video.slow ? this.subtitles[i].timeCode_Slow.end / 1000 : this.subtitles[i].timeCode_Normal.end / 1000

        if (time >= begin && end > time) {
          return i
        }
      }

      return this.video.currentSentIdx
    },

    jumpToSent (sent) {
      var seconds = this.video.slow ? sent.timeCode_Slow.begin / 1000 : sent.timeCode_Normal.begin / 1000

      if (seconds >= 0) {
        this.video.el.currentTime = seconds
      }
    },

    nextSubtitle () {
      this.repeat = false
      if (this.video.currentSentIdx < this.subtitles.length - 1) {
        this.jumpToSent(this.subtitles[this.video.currentSentIdx + 1])
      }
    },

    preSubtitle () {
      this.repeat = false
      if (this.video.currentSentIdx > 0) {
        this.jumpToSent(this.subtitles[this.video.currentSentIdx - 1])
      }
    },

    repeatSingle () {
      this.repeatIdx = this.video.currentSentIdx
      this.repeat = !this.repeat
    }
  }
}
