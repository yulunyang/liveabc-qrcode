export default {
  data () {
    return {
      typeList: [
        { type: 'magazine', title: 'ABC互動英語', titleEn: 'ABC Magazine', logo: 'logo-abc.png', cover: 'logo-abc.png', url: 'https://www.liveabc.com' },
        { type: 'magazine', title: 'ALL+互動英語', titleEn: 'ALL+ Magazine', logo: 'logo-all.png', cover: 'logo-all.png', url: 'https://www.liveabc.com' },
        { type: 'magazine', title: 'LIVE互動英語', titleEn: 'LIVE Magazine', logo: 'logo-live.png', cover: 'logo-live.png', url: 'https://www.liveabc.com' },
        { type: 'magazine', title: '互動日本語', titleEn: 'Japanese Magazine', logo: 'logo-jp.png', cover: 'logo-jp.png', url: 'https://www.liveabc.com' },
        { type: 'magazine', title: 'CNN互動英語', titleEn: 'CNN Magazine', logo: 'logo-cnn.png', cover: 'logo-cnn.png', url: 'https://www.liveabc.com' },
        { type: 'magazine', title: 'biz互動英語', titleEn: 'biz Magazine', logo: 'logo-biz.png', cover: 'logo-biz.png', url: 'https://www.liveabc.com' }
      ],
      unitHint: [
        { id: 1, name: '關鍵時事新聞' },
        { id: 2, name: '進階閱讀(1)' },
        { id: 3, name: '進階閱讀(2)' },
        { id: 4, name: '進階閱讀(3)' },
        { id: 5, name: '英文慣用語' },
        { id: 6, name: '學校沒教的潮英語' },
        { id: 7, name: '畫中有話' },
        { id: 8, name: '本月之星' },
        { id: 9, name: '電影速報' },
        { id: 10, name: '每日一句' },
        { id: 11, name: '全民英檢初級模擬試題' }
      ],
      sectionHint: [
        { id: 1, name: 'Part 1' },
        { id: 2, name: 'Part 2' },
        { id: 3, name: 'Part 3' },
        { id: 4, name: '圖解字典' },
        { id: 5, name: '電影速報' },
        { id: 6, name: '本月之星' },
        { id: 7, name: '畫中有話' },
        { id: 8, name: '進階閱讀' },
        { id: 9, name: '每日一句' },
        { id: 10, name: '全民英檢初級模擬試題' }
      ],
      partHint: [
        { id: 1, name: '課文朗讀' },
        { id: 2, name: '單字朗讀' },
        { id: 3, name: '課程講解' },
        { id: 4, name: '看圖辨義' },
        { id: 5, name: '問答' },
        { id: 6, name: '簡短對話' },
        { id: 7, name: '題目朗讀' },
        { id: 8, name: '單字例句' },
        { id: 9, name: '情境影片' },
        { id: 10, name: '聽力測驗' },
        { id: 11, name: 'CNN影片' },
        { id: 12, name: '影片原音' },
        { id: 13, name: '慢速朗讀' }
      ]
    }
  }
}
