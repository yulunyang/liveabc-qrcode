var langMixin = {

  methods: {
    toJap (sent, hidePinyin = false) {
      this.forceToJap(sent, hidePinyin)
    },

    forceToJap (sent, hidePinyin = false) {
      let split = sent.split(/{(.+?)}/)
      let newSent = ''
      for (let i in split) {
        if (split[i].indexOf('=') > -1) {
          if (!hidePinyin) {
            newSent += '<ruby>' + split[i].split(/=/)[0] + '<rt>' + split[i].split(/=/)[1] + '</rt></ruby>'
          } else {
            newSent += split[i].split(/=/)[0]
          }
        } else {
          newSent += split[i]
        }
      }
      return newSent
    },

    rubyParsing (data) {
      if (typeof data === 'string') {
        return this.forceToJap(data)
      } else if (Array.isArray(data)) {
        for (let i = data.length - 1; i >= 0; i--) {
          data[i] = this.rubyParsing(data[i])
        }
      } else if (data && typeof data === 'object') {
        for (let k in data) {
          data[k] = this.rubyParsing(data[k])
        }
      }

      return data
    },

    jsonDataParsingRuby (data) {
      return this.rubyParsing(data)
    },

    getOnlyJp (sent, hidePinyin = false) {
      let split = sent.split(/{(.+?)}/)
      let newSent = ''
      for (let i in split) {
        if (split[i].indexOf('=') > -1) {
          if (!hidePinyin) {
            newSent += split[i].split(/=/)[1]
          } else {
            newSent += split[i].split(/=/)[0]
          }
        } else {
          newSent += split[i]
        }
      }
      return newSent
    },

    checkJPN (sent) {
      var re1 = new RegExp('^[\u0800-\\u4e00]*$')

      if (sent === '') {
        return false
      }

      var testChar = ''

      var rc
      for (let i = 0; i < sent.length; i++) {
        testChar = sent.substr(i, 1)

        if (!(re1.test(testChar))) {
          rc = false
        } else {
          return true
        }
        if (rc === false) {
          return false
        } else {
          return true
        }
      }
    }
  }
}

export default langMixin
