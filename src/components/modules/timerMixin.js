import moment from 'moment'
import 'moment-duration-format'

export default {

  data () {
    return {
      countDownState: true
    }
  },

  created () {
    var requestAnimationFrame = window.requestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.msRequestAnimationFrame

    window.requestAnimationFrame = requestAnimationFrame
  },

  beforeDestroy () {
    this.stopCountDown()
  },

  methods: {
    duration: function (seconds) {
      return moment.duration(seconds, 'minutes').format('hh:mm', { trim: false })
    },

    startCountDown (seconds, cb) {
      seconds = seconds || 600
      cb = cb || null

      var start = null
      var timer = 0
      var step = (timestamp) => {
        var progress

        if (start === null) {
          start = timestamp
        }

        progress = timestamp - start

        timer = seconds - Math.round(progress / 1000)

        if (cb !== null && typeof cb === 'function') {
          cb(timer)
        }

        if (this.countDownState && timer > 0) {
          window.requestAnimationFrame(step)
        }
      }

      window.requestAnimationFrame(step)
    },

    stopCountDown () {
      this.countDownState = false
    }
  }
}
