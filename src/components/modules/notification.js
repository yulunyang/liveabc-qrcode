import AWN from 'awesome-notifications'

export default {
  methods: {
    // 成功
    notification_Success (val) {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        success: 1200
      }
      notifier.success(val, options)
    },

    // 重複/未填
    notification_Attention (val) {
      // eslint-disable-next-line no-unneeded-ternary
      let text = val ? val : '有欄位未填寫或名稱重複'
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        warning: 1500
      }
      notifier.warning(text, options)
    },

    // 有空值
    notification_Info () {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        info: 1500
      }
      notifier.info('請至少新增一個項目', options)
    },

    // 儲存
    notification_save () {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        info: 900
      }
      notifier.info('Save!', options)
    },

    notification_version () {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        warning: 2000
      }
      notifier.warning('版本不是最新的!', options)
    }
  }
}
