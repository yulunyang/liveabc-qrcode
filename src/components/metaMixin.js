export default {
  metaInfo () {
    return {
      title: this.page_title ? this.page_title : ''
    }
  },

  data () {
    return {
      page_title: ''
    }
  },

  watch: {
    // $route (to, from) {
    //   this.$ga.page({
    //     page: this.$router.path,
    //     title: this.page_title,
    //     location: window.location.href
    //   })
    // }
  },

  updated () {
    if (this.data) {
      this.page_title = this.getPageTitle()
    }
  },

  methods: {
    getPageTitle () {
      let self = this
      let indexOfNum = 0

      if (self.$route.params.id) {
        let routeIdString = this.$route.params.id.toString()
        let indexofarray = []
        for (let i = 0; i < self.content.length; i++) {
          indexofarray.push(self.content[i].id)
        }
        indexOfNum = indexofarray.indexOf(routeIdString)
      }

      let publishDate = self.data.publish_date ? self.data.publish_date + ' ' : ''

      return (self.$route.params.id ? self.data.content[indexOfNum].name + ' | ' : '') + publishDate + self.data.title
    },

    updatePageTitle () {
      this.page_title = this.getPageTitle()

      // setTimeout(() => {
      //   this.$ga.page({
      //     page: this.$router.path,
      //     title: this.title,
      //     location: window.location.href
      //   })
      // }, 1000)
    }
  }
}
