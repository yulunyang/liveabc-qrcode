import axios from 'axios'
import store from './store/index'
import router from './router/index'

axios.defaults.baseURL = 'https://api.liveabc.com/v1'
axios.defaults.timeout = 60000

// axios.defaults.retry = 4
// axios.defaults.retryDelay = 1000

axios.interceptors.request.use(request => {
  store.dispatch('updateLoading', true)
  const token = localStorage.getItem('token')
  token ? request.headers.common['Authorization'] = `Bearer ${token}` : null
  axios.defaults.withCredentials = false

  return request
})

// Response interceptor
axios.interceptors.response.use(response => {
  if (response.data.error && response.data.error === 'TOKEN_EXPIRED') {
    store.dispatch('refreshToken')
  }
  if (response.data.error && response.data.error === 'TOKEN_INVALID') {
    store.dispatch('logout')
    router.push({ name: 'login' })
  }
  store.dispatch('updateLoading', false)
  return response
}, error => {
  store.dispatch('updateLoading', false)

  const { status } = error.response
  if (status === 401) {
    alert('請重新登入')
    store.dispatch('logout')
  } else if (status === 429) {
    window.alert('系統忙碌中，請稍後再試。')
  } else if (status === 500) {
    store.commit('setErrorMessage', error.response)
  } else if (error.response) {
    store.commit('setErrorMessage', error.response)
  } else {
    store.commit('setErrorMessage', error.message)
  }

  return Promise.reject(error)
})
