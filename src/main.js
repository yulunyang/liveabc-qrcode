import Vue from 'vue'
import App from './App'
import router from './router'
import '../node_modules/bootstrap/scss/bootstrap.scss'
import 'font-awesome/css/font-awesome.css'
import './assets/app.scss'
import '../static/css/magazine.scss'
// import VueAnalytics from 'vue-analytics'
import VueMeta from 'vue-meta'
import store from './store/index'
import './api'
import 'vue-loading-overlay/dist/vue-loading.css'
import VueAWN from 'vue-awesome-notifications'
Vue.use(VueAWN)

import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
import 'vue-select/dist/vue-select.css'

import Vuex from 'vuex'
Vue.use(Vuex)
window.$ = window.jQuery = require('jquery')

// moment
import moment from 'moment'
Vue.prototype.moment = moment

// Loading
import Loading from 'vue-loading-overlay'
require('../static/css/font-notification/style.scss')
Vue.component('Loading', Loading)

import VueI18n from 'vue-i18n'
import zh from './i18n/zh'
import en from './i18n/en'
Vue.use(VueI18n)
let locale = 'en'

var langJudge = window.navigator.userLanguage || window.navigator.language
if (langJudge === 'zh-TW') {
  locale = 'zh'
  store.dispatch('setLang', 'zh')
} else {
  locale = 'en'
  store.dispatch('setLang', 'en')
}
import VueGtm from 'vue-gtm'
Vue.use(VueGtm, {
  id: ['GTM-KT6QNHX'],
  enabled: process.env.NODE_ENV === 'production',
  defer: false,
  debug: true,
  loadScript: true,
  vueRouter: router,
  ignoredViews: ['index', 'adminlogin', 'list', 'edit']
})

const i18n = new VueI18n({
  locale: locale,
  messages: {
    'zh': zh,
    'en': en
  }
})

Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
})

// Vue.use(VueAnalytics, {
//   id: 'UA-78537321-5',
//   autoTracking: {
//     pageviewOnLoad: false
//   },
//   debug: {
//     enabled: false
//   }
// })

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  store,
  template: '<App/>',
  components: { App }
})
