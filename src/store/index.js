import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import AWN from 'awesome-notifications'

Vue.use(Vuex)

const state = {
  auth: {
    authorized: false,
    userRole: '',
    user: null
  },
  isLoading: false,
  errorMessage: '',
  ebooks: '',
  lastModified: null,
  lang: 'en'
}

const getters = {
  user: state => state.auth.user,
  account: state => state.auth.user.name,
  authorized: state => state.auth.authorized,
  userRole: state => state.auth.userRole,
  ebooks: state => state.ebooks,
  isLoading: state => state.isLoading,
  errorMessage: state => state.errorMessage,
  lastModifiedTime: state => state.lastModified,
  getDateTime: () => {
    let thisYear = new Date().getFullYear()
    let beginYear = 2014
    let gap = (thisYear - beginYear) - 5

    let yearAr = []
    for (let i = 0; i < 10 + gap; i++) {
      yearAr.push(beginYear + i)
    }
    return yearAr
  },
  lang: state => state.lang
}

const actions = {
  setLang: async ({ commit }, data) => {
    commit('SETLANG', data)
  },

  updateLoading (context, status) {
    context.commit('LOADING', status)
  },

  login: async ({ commit, dispatch }, user) => {
    const { data } = await axios.post('/login', {
      'email': user.account,
      'password': user.password
    })
    commit('LOGIN', data)
    dispatch('getProfile')
  },

  useTokenLogin: ({ commit, dispatch }, token) => {
    let loginToken = { access_token: token, expires_in: 28800, token_type: 'bearer' }
    commit('LOGIN', loginToken)
    dispatch('getProfile')
  },

  getProfile: async ({ commit }) => {
    const { data } = await axios.get('/user/me')
    commit('UPDATEPROFILE', data)
  },

  logout: async ({ commit }) => {
    return await axios.post('/logout')
      .then((response) => {
        commit('LOGOUT')
        window.location = '/adminlogin'
      }).catch(() => {
        commit('LOGOUT')
        window.location = '/adminlogin'
      })
  },

  dataPost: async ({ commit }, jsonData) => {
    await axios.post('/ebooks/', {
      'id': jsonData.id,
      'data': jsonData.data
    }).then((response) => {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        info: 1000
      }
      notifier.info('Save!', options)
      commit('LASTMODIFIED', (Date.parse(response.data['last-modified'])).valueOf())
    }).catch((error) => {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        alert: 1500
      }
      notifier.alert('Warn!:' + error, options)
    })
  },

  dataPatch: async ({ commit }, jsonData) => {
    await axios.patch(`/ebooks/${jsonData.id}`, {
      'data': jsonData.data
    }).then((response) => {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        info: 1000
      }
      notifier.info('Save!', options)

      // 更新我的版本
      commit('LASTMODIFIED', (Date.parse(response.data['last-modified'])).valueOf())
    }).catch(() => {
      let notifier = new AWN(options)
      let options = {}
      options.durations = {
        alert: 2000
      }
      notifier.alert('存檔錯誤!', options)
    })
  },

  refreshToken: async () => {
    let { data } = await axios.get('/user/token/refresh')
    localStorage.setItem('token', data.access_token)
    window.location.reload()
  }
}
const mutations = {
  LOADING (state, status) {
    state.isLoading = status
  },

  LOGIN (state, data) {
    state.auth.authorized = true
    state.auth.user = data
    localStorage.setItem('token', data.access_token)
  },

  UPDATEPROFILE (state, data) {
    state.auth.userRole = data.role
    state.auth.user = {
      'id': data.id,
      'email': data.email,
      'name': data.name
    }
  },

  LOGOUT (state) {
    state.auth.authorized = false
    state.auth.user = {}
    state.auth.userRole = ''
    localStorage.removeItem('token')
  },

  setErrorMessage (state, message) {
    state.errorMessage = message
  },

  LASTMODIFIED (state, data) {
    state.lastModified = data
  },

  SETLANG (state, value) {
    state.lang = value
  }

}
const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  modules: {
  },
  strict: debug,
  plugins: [createPersistedState({
    paths: [
      'auth'
    ]
  })]
})
