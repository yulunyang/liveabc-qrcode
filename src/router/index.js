import Vue from 'vue'
import store from '../store/index'
import Router from 'vue-router'
import Book from '@/components/Book'
import Magazine from '@/components/Magazine'
import Backstage from '@/components/Backstage'
import Login from '@/components/backstage/LoginSection'
import List from '@/components/backstage/ListSection'
import Edit from '@/components/backstage/EditSection'
import NoWorkingPage from '@/components/modules/NoWorkingPage'

Vue.use(Router)

let routes = [
  {
    path: '/0120328/play/:unit?/:lesson?',
    redirect: '/book/0120328/:unit?/:lesson?'
  },
  {
    path: '/basic-grammar/play/:unit/:lesson',
    redirect: '/book/1100204/:unit?/:lesson?'
  },
  {
    path: '/ebook/:product_no/play/:unit?/:lesson?',
    redirect: '/book/:product_no/:unit?/:lesson?'
  },
  {
    path: '/book/:product_no/play/:unit?/:lesson?',
    redirect: '/book/:product_no/:unit?/:lesson?'
  },
  {
    path: '/magzine/:book/:id?/:section?/:parts?',
    redirect: '/magazine/:book/:id?/:section?/:parts?'
  },
  {
    path: '/book/:product_no/:unit?/:lesson?',
    name: 'Book',
    component: Book
  },
  {
    path: '/magazine/:book/:id?/:section?/:parts?',
    name: 'magazine',
    component: Magazine
  },
  {
    path: '/adminlogin',
    name: 'adminlogin',
    component: Login,
    meta: { requiresAuth: false }
  },
  {
    path: '/management',
    name: 'management',
    component: Backstage,
    children: [
      {
        path: 'list',
        name: 'list',
        component: List,
        meta: { requiresAuth: true }
      },
      {
        path: 'edit/:book?',
        name: 'edit',
        component: Edit,
        meta: { requiresAuth: true }
      }
    ]
  },
  {
    path: '*',
    name: 'index',
    component: NoWorkingPage
  }
]

const router = new Router({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.authorized) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    if (to.path === '/adminlogin' && store.getters.authorized) {
      next({ path: '/management/list' })
    } else {
      next()
    }
  }
})

export default router
