// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  "plugins": {
    // to edit target browsers: use "browserslist" field in package.json
    "autoprefixer": {}
  },
  loaders: [
    {
      test: require.resolve('jquery'),
      loader: 'expose?jQuery!expose?$'
    }
  ]
}
